package com.example.registro_recuperacion.Controlador;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class Singleton {

    private RequestQueue queue;
    private Context context;
    private static Singleton myInstance;

    public Singleton(Context context){
        this.context = context;

        queue = getRequestQueue();
    }

    public RequestQueue getRequestQueue(){
        if(queue == null)
            queue = Volley.newRequestQueue(context.getApplicationContext());

        return queue;
    }

    public static synchronized Singleton getInstance(Context context){
        if(myInstance == null)
            myInstance = new Singleton(context);

        return myInstance;
    }

    public <T> void addToRequestQueue(Request request){
        queue.add(request);
    }

}
