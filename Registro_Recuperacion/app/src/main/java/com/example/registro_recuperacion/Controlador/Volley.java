package com.example.registro_recuperacion.Controlador;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class Volley {

    private String host = "http://localhost:8000";
    private String regi = "/cliente";
    private String alqu = "/alquiler";
    private String log = "/cliente/login";

    Context context;

    public Volley(Context contexto){
        this.context = contexto;
    }

    public void logear(String c, String p, final sync s){

        String url = host.concat(log);
        JSONObject json = new JSONObject();

        try {
            json.put("cedula", c);
            json.put("password", p);
        }catch (JSONException e){
            Log.e("Error json", "no se pudo insertar datos en el json");
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                s.response(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "No se pudo obtener datos", Toast.LENGTH_SHORT).show();
                String er = error.getMessage();
                Log.e("mal", er);
            }
        });

        Singleton.getInstance(context).addToRequestQueue(request);

    }

    public void registrarser(String c, String n, String a, String p, final sync s){
        String url = host.concat(regi);
        final JSONObject json = new JSONObject();

        try {
            json.put("cedula", c);
            json.put("nombre", n);
            json.put("apellido", a);
            json.put("password", p);
        }catch (JSONException e){
            Log.e("Error json", "no se pudo insertar datos en el json");
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                s.response(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "No se pudo obtener datos", Toast.LENGTH_SHORT).show();
                String er = error.getMessage();
                Log.e("mal", er);
            }
        });

        Singleton.getInstance(context).addToRequestQueue(request);

    }
}
