package com.example.registro_recuperacion.vista;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.registro_recuperacion.Controlador.Volley;
import com.example.registro_recuperacion.Controlador.sync;
import com.example.registro_recuperacion.MainActivity;
import com.example.registro_recuperacion.Modelo.Usuario;
import com.example.registro_recuperacion.R;

import org.json.JSONException;
import org.json.JSONObject;

public class logear extends AppCompatActivity implements View.OnClickListener {

    EditText usn, pas;
    TextView registro;
    Button ingreso;
    Usuario u;
    Volley vo = new Volley(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logear);
        controlar();
    }

    public void controlar(){
        usn.findViewById(R.id.username);
        pas.findViewById(R.id.txt_password);
        registro = findViewById(R.id.goRegistro);
        ingreso = findViewById(R.id.login);
        ingreso.setOnClickListener(this);
        registro.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.login:
                String usr = usn.getText().toString();
                String clave = pas.getText().toString();

                if(usr.equals("") || clave.equals("")){
                    Toast.makeText(this, "No se han ingresado datos", Toast.LENGTH_SHORT).show();
                }else {
                    vo.logear(usr, clave, new sync() {
                        @Override
                        public void response(JSONObject json) {
                            try {
                                boolean sta = json.getBoolean("success");
                                if(sta){
                                    Toast.makeText(logear.this, json.getString("information"), Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(logear.this, MainActivity.class);
                                    intent.putExtra("user", json.getJSONObject("data").getString("cedula"));
                                    startActivity(intent);
                                }else{
                                    Toast.makeText(logear.this, "No registrado", Toast.LENGTH_SHORT).show();
                                }
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    });
                }
                break;
            case R.id.goRegistro:
                Intent intent = new Intent(logear.this, registrarse.class);
                startActivity(intent);
                break;
        }
    }
}
