package com.example.registro_recuperacion.vista;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.registro_recuperacion.Controlador.Volley;
import com.example.registro_recuperacion.Controlador.sync;
import com.example.registro_recuperacion.R;

import org.json.JSONException;
import org.json.JSONObject;

public class registrarse extends AppCompatActivity implements View.OnClickListener {

    EditText name, pass, emal, ape;
    Button registrar;
    TextView golog;
    Volley h = new Volley(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrarse);

        control();
    }

    public void control(){
        name = findViewById(R.id.txt_nombre);
        pass = findViewById(R.id.txt_clave);
        emal = findViewById(R.id.txt_correo);
        ape = findViewById(R.id.txt_apellido);
        registrar = findViewById(R.id.btn_registrarse);
        golog = findViewById(R.id.goLogin);
        golog.setOnClickListener(this);
        registrar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;

        switch (view.getId()){
            case R.id.goLogin:
                intent = new Intent(registrarse.this, logear.class);
                startActivity(intent);
                break;

            case R.id.btn_registrarse:
                registro();
                break;
        }
    }

    private void registro(){
        String nom = name.getText().toString();
        String pas = pass.getText().toString();
        String em = emal.getText().toString();
        String ap = ape.getText().toString();

        if(nom.equals("") || pas.equals("") || em.equals("") || ap.equals("")){
            Toast.makeText(this, "Ingresa datos primero", Toast.LENGTH_SHORT).show();
        }else {
            h.registrarser(em, nom, ap, pas, new sync() {
                @Override
                public void response(JSONObject json) {
                    try {
                        if (json.getBoolean("success")) {
                            Toast.makeText(registrarse.this, json.getString("information"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(registrarse.this, logear.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(registrarse.this, "Correo ya registrado", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }


}

