<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Alquiler extends Model
{
    
    protected $table = 'modelo_alquiler';
    protected $primaryKey = 'alquiler_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'alquiler_id', 'cantidad_dias', 'cedula', 'createdAt', 'auto_id', 'cliente_id'
    ];

    public $timestamps = false;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}