<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Auto extends Model
{
    
    protected $table = 'modelo_auto';
    protected $primaryKey = 'auto_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'auto_id', 'placa', 'marca', 'modelo', 'costo', 'estado'
    ];

    public $timestamps = false;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}