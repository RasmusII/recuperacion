<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Cliente extends Model
{
    
    protected $table = 'modelo_cliente';
    protected $primaryKey = 'cliente_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cliente_id', 'nombre', 'apellido', 'cedula', 'password'
    ];

    public $timestamps = false;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}