<?php

namespace App\Http\Controllers;

use App\Alquiler;
use DateTime;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;


class AlquilerController extends BaseController
{
    public function all(Request $request){
        $Alquiler = Alquiler::all();
        return response() ->json($Alquiler, 200);
    }

    public function getAlquiler(Request $request, $alquiler_id){
        if($request -> isJson()){
            $Alquiler = Alquiler::where('alquiler_id', $alquiler_id) -> get();
            if(!$Alquiler -> isEmpty()){
                $status = true;
                $info = "Data listada exitosamente";
            }
            else{
                $status = false;
                $info = "Data no listada exitosamente";
            }
            return ResponseBuilder::result($status, $info, $Alquiler);
        }
        else{
            $status = false;
    		$info = "Unauthorized";	
    		return ResponseBuilder::result($status, $info);
        }
    }

    public function createAlquiler(Request $request){
        $Alquiler = new Alquiler();
        $aux = new DateTime('now');

        $Alquiler -> cantidad_dias = $request->cantidad_dias;
        $Alquiler -> cedula = $request->cedula;
        $Alquiler -> createdAt = Date($aux->format('Y-m-d'));
        $Alquiler -> auto_id = $request->auto_id;
        $Alquiler -> cliente_id = $request->cliente_id;

        $Alquiler -> save();

        $info = 'Alquiler creado correctacmente';
    	$status = true;

    	return(ResponseBuilder::result($status, $info, $Alquiler));
    }

    public function modificar(Request $request, $alquiler_id){
        if($request -> isJson()){
            $aux = $request -> all();
            Alquiler::where('alquiler_id',$alquiler_id) -> update($aux);
            $Alquiler = Alquiler::where('alquiler_id',$alquiler_id) -> first();
            if($Alquiler != null){
                $status = true;
                $info = "Data is modified successfuly";
            }
            else{
                $status = false;
                $info = "Data is not modified successfuly";   
            }
            return ResponseBuilder::result($status, $info, $Alquiler);
        }
        else{
            $status = false;
            $info = "Unauthorized"; 
            return ResponseBuilder::result($status, $info);
        }
    }
}