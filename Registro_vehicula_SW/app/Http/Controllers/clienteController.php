<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class clienteController extends BaseController{
    public function all(Request $request){
        $Cliente = Cliente::all();
        return response() ->json($Cliente, 200);
    }

    public function getCliente(Request $request, $cedula){
        if($request -> isJson()){
            $Cliente = Cliente::where('cedula', $cedula)->get();
            if(!$Cliente -> isEmpty()){
                $status = true;
                $info = "Data listada exitosamente";
            }
            else{
                $status = false;
                $info = "Data no listada exitosamente";
            }
            return ResponseBuilder::result($status, $info, $Cliente);
        }
        else{
            $status = false;
    		$info = "Unauthorized";	
    		return ResponseBuilder::result($status, $info);
        }
    }

    public function createCliente(Request $request){
        $Cliente = new Cliente();        
        
        $Cliente -> cedula = $request->cedula;
        $Cliente -> nombre = $request->nombre;
        $Cliente -> apellido = $request->apellido;
        $Cliente -> password = $request->password;

        $Cliente -> save();

        $info = 'Cliente creado correctacmente';
    	$status = true;

    	return(ResponseBuilder::result($status, $info, $Cliente));
    }

    public function modificar(Request $request, $cedula){
        if($request -> isJson()){
            $aux = $request -> all();
            Cliente::where('cedula',$cedula) -> update($aux);
            $Cliente = Cliente::where('cedula',$cedula) -> first();
            if($Cliente != null){
                $status = true;
                $info = "Data is modified successfuly";
            }
            else{
                $status = false;
                $info = "Data is not modified successfuly";   
            }
            return ResponseBuilder::result($status, $info, $Cliente);
        }
        else{
            $status = false;
            $info = "Unauthorized"; 
            return ResponseBuilder::result($status, $info);
        }
    }

    public function login(Request $request){
        if($request->isJson()){
            $cedula = $request ->cedula;
            $password = $request ->password;

            $Cliente = Cliente::where('cedula', $cedula)->first();
            if(!empty($Cliente)){
                if($Cliente->password == $password){
                    $info = 'Credenciales Correctas';
                    $status = true;
                    return ResponseBuilder::result($status, $info, $Cliente);                    
                }
                else{
                    $info = 'Credenciales Incorrectas';
                    $status = false;
                    $Cliente = '';
                }
            }else{
                $info = 'Credenciales Incorrectas';
                $status = false;
                $Cliente = '';
            }
        }
        else{
            $status = false;
            $info = "Unauthorized"; 
            $Cliente = '';
        }
        return ResponseBuilder::result($status, $info, $Cliente);
    }
}