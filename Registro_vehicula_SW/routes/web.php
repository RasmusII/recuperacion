<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router -> group(['prefix' => 'cliente'], function($router){
    $router->get('', 'clienteController@all');
    $router->post('', 'clienteController@createCliente');
    $router->get('{cedula}', 'clienteController@getCliente');
    $router->post('{cedula}', 'clienteController@modificar');
    $router->post('login', 'clienteController@login');
});

$router -> group(['prefix' => 'alquiler'], function($router){
    $router->get('', 'alquilerController@all');
    $router->post('', 'alquilerController@createAlquiler');
    $router->get('{alquiler_id}', 'alquilerController@getAliente');
    $router->post('{alquiler_id}', 'alquilerController@modificar');
});