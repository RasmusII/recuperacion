from django import forms;
from app.modelo.models import Cliente;
from app.modelo.models import Auto;
from app.modelo.models import Alquiler;

class FormularioAuto(forms.ModelForm):
    class Meta:
        model = Auto
        fields = ["placa", "marca", "modelo", "costo"]

class FormularioCliente(forms.ModelForm):
    class Meta:
        model = Cliente
        fields = ["nombre", "apellido", "cedula", "password"]

class FormularioAlquiler(forms.ModelForm):
    class Meta:
        model = Alquiler
        fields = ["cantidad_dias", "cedula"]

class FormularioLogin(forms.Form):
	username = forms.CharField()
	password = forms.CharField(widget = forms.PasswordInput())