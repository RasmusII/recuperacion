# Generated by Django 2.2.5 on 2020-03-20 16:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('modelo', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='alquiler',
            old_name='auto_id',
            new_name='auto',
        ),
        migrations.RenameField(
            model_name='alquiler',
            old_name='cliente_id',
            new_name='cliente',
        ),
    ]
