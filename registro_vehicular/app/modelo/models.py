from django.db import models

# Create your models here.
class Auto(models.Model):
    auto_id = models.AutoField(primary_key = True)
    placa = models.CharField(max_length = 10, null = False, unique = True)
    marca = models.CharField(max_length = 20, null = False)
    modelo = models.CharField(max_length = 10, null = False)
    costo = models.DecimalField(max_digits = 5, decimal_places = 2, null = False)
    estado = models.CharField(max_length = 15, null = False)

class Cliente(models.Model):
    cliente_id = models.AutoField(primary_key = True)
    nombre = models.CharField(max_length = 30, null = False)
    apellido = models.CharField(max_length = 30, null = False)
    cedula = models.CharField(max_length = 10, null = False, unique = True)
    password = models.CharField(max_length = 20, null = False)

class Alquiler(models.Model):
    alquiler_id = models.AutoField(primary_key = True)
    cantidad_dias = models.IntegerField(null = False)
    cedula = models.CharField(max_length = 10, null = False)
    createdAt = models.DateField('CreatedAt', auto_now=True, auto_now_add=False)
    auto = models.ForeignKey('Auto', on_delete = models.CASCADE)
    cliente = models.ForeignKey('Cliente', on_delete = models.CASCADE)