from django.urls import path

from . import views

urlpatterns = [
	path('', views.principal, name = 'principal'),
    path('login', views.ingresar, name='autenticar'),	
	path('logout', views.cerrar, name='cerrar_sesion'),
    path('autos', views.listar_autos, name = 'autos'),
    path('crearAuto', views.crearAuto, name= 'crearAuto'),
    path('clientes', views.listar_clientes, name = 'clientes'),
    path('crearCliente', views.crearCliente, name= 'crearCliente'),
    path('alquiler', views.listar_alquiler, name = 'alquiler'),
    path('crearAlquiler', views.crearAlquiler, name= 'crearAlquiler'),
    path('detalle', views.detalle, name = 'detalle'),
]