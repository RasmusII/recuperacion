from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from .Forms import FormularioAlquiler, FormularioAuto, FormularioCliente, FormularioLogin
from app.modelo.models import Alquiler
from app.modelo.models import Auto
from app.modelo.models import Cliente


# Create your views here.
def ingresar(request):     
    if request.method == 'POST':
        formulario = FormularioLogin(request.POST)
        if formulario.is_valid():
            usuario = request.POST['username']
            password = request.POST['password']
            user = authenticate(username = usuario, password = password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect(reverse('principal'))
                else:
                    print ('usuario desactivado')
            else:
                print ('credenciales no validads')
    else:
        formulario = FormularioLogin()
    context = {
        'f': formulario
    }
    return render(request, 'login/login.html', context)

def cerrar(request):
    logout(request)

    return HttpResponseRedirect(reverse('autenticar'))

@login_required(login_url = '/login')
def principal(request):
    return render(request, 'principal.html')

#listar los modelos
@login_required(login_url = '/login')
def listar_autos(request):
    lista = Auto.objects.all()
    context = {
        'lista' : lista
    }
    return render(request, 'auto/auto.html', context)

@login_required(login_url = '/login')
def listar_clientes(request):
    lista = Cliente.objects.all()
    context = {
        'lista' : lista
    }
    return render(request, 'cliente/cliente.html', context)

@login_required(login_url = '/login')
def listar_alquiler(request):
    lista = Alquiler.objects.all()
    context = {
        'lista' : lista
    }
    return render(request, 'alquiler/alquiler.html', context)

# CREACION
@login_required(login_url = '/login')
def crearAuto(request):
    usuario = request.user
    formularioAuto = FormularioAuto(request.POST)
    if usuario.groups.filter(name = 'gerente').exists():
        if request.method == 'POST':
            if formularioAuto.is_valid():
                datos = formularioAuto.cleaned_data
                auto = Auto()
                auto.costo = datos.get('costo')
                auto.estado = 'Disponible'
                auto.marca = datos.get('marca')
                auto.modelo = datos.get('modelo')
                auto.placa = datos.get('placa')

                auto.save()
                return redirect(listar_autos)
    else:
        return redirect(principal)
    context = {
        'fa' : formularioAuto
    }

    return render(request, 'auto/crearAuto.html', context)

@login_required(login_url = '/login')
def crearCliente(request):
    usuario = request.user
    formularioCliente= FormularioCliente(request.POST)
    if usuario.groups.filter(name = 'administrativo').exists():
        if request.method == 'POST':
            if formularioCliente.is_valid():
                datos = formularioCliente.cleaned_data
                cliente = Cliente()
                cliente.apellido = datos.get('apellido')
                cliente.cedula = datos.get('cedula')
                cliente.nombre = datos.get('nombre')
                cliente.password = datos.get('password')

                cliente.save()
                return redirect(listar_clientes)
    else:
        return redirect(principal)

    contex = {
        'fc' : formularioCliente
    }

    return render(request, 'cliente/crearCliente.html', contex)

@login_required(login_url = '/login')
def crearAlquiler(request):
    usuario = request.user
    id = request.GET['auto_id']    
    formularioAlquiler= FormularioAlquiler(request.POST)
    if usuario.groups.filter(name = 'empleado').exists():
        if request.method == 'POST':
            if formularioAlquiler.is_valid():
                datos = formularioAlquiler.cleaned_data
                dni = datos.get('cedula')                
                try:
                    cliente = Cliente.objects.get(cedula = dni)
                except Cliente.DoesNotExist:
                    return redirect(crearAlquiler)

                alquiler = Alquiler()                
                alquiler.cantidad_dias = datos.get('cantidad_dias')
                alquiler.cedula = dni
                alquiler.auto_id = id
                alquiler.cliente_id = cliente.cliente_id

                alquiler.save()
                return redirect(listar_alquiler)
    else:
        return redirect(principal)

    contex = {
        'fal' : formularioAlquiler
    }

    return render(request, 'alquiler/crearAlquiler.html', contex)

@login_required(login_url = '/login')
def detalle(request):
    usuario = request.user
    id = request.GET['alquiler_id']
    if usuario.groups.filter(name = 'gerente').exists():
        obje = Alquiler.objects.get(alquiler_id = id)
    else:
        return redirect(principal)
    context = {
        'obj' : obje
    }
    return render(request, 'alquiler/detalle.html', context)